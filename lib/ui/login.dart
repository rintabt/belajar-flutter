import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:login/constants/preferences.dart';
import 'package:login/routes.dart';
import 'package:login/stores/form_store.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _nrpController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  FocusNode _passwordFocusNode;
  final _formKey = GlobalKey<FormState>();
  final _store = FormStore();

  bool _isHidden = true;

  _toogleIsHidden() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  void initState() {
    super.initState();

    _passwordFocusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: _buildBody()));
  }

  Widget _buildBody() {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            children: <Widget>[
              _buildNrpField(),
              _buildPasswordField(),
              _buildSignInButton()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNrpField() {
    return Observer(builder: (context) {
      return TextFormField(
        controller: _nrpController,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
            hintText: "NRP",
            icon: Icon(Icons.person),
            errorText: _store.formErrorStore.nrp),
        keyboardType: TextInputType.number,
        onChanged: (value){
          _store.setNrp(_nrpController.text);
        },
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordFocusNode);
        },
      );
    });
  }

  Widget _buildPasswordField() {
    return Observer(builder: (_) {
      return TextFormField(
        controller: _passwordController,
        obscureText: _isHidden,
        focusNode: _passwordFocusNode,
        decoration: InputDecoration(
            hintText: "Password",
            errorText: _store.formErrorStore.password,
            icon: Icon(Icons.lock),
            suffixIcon: IconButton(
              onPressed: _toogleIsHidden,
              icon: _isHidden
                  ? Icon(Icons.visibility_off)
                  : Icon(Icons.visibility),
            )),
        keyboardType: TextInputType.text,
        onChanged: (value){
          _store.setPassword(_passwordController.text);
        },
      );
    });
  }

  Widget _buildSignInButton() {
    return Container(
        margin: EdgeInsets.only(top: 10.0),
        width: double.infinity,
        child: FlatButton(
          color: Colors.orange,
          shape: StadiumBorder(),
          child: Text("Sign In", style: TextStyle(color: Colors.white)),
          onPressed: () {
            _validationForm();
          },
        ));
  }

  _isUserInvalid(String nrp, String password) {
    //**Validasi dari data BE
    if (nrp == (1111).toString() && password == "password") {
      return false;
    }
    return true;
  }

  _validationForm() {
    if (!_store.canLogin) {
      _showErrorMessage("Isi form belum lengkap");
    } else if (_isUserInvalid(_nrpController.text, _passwordController.text)) {
      _showErrorMessage("NRP atau password invalid");
    } else {
      FocusScope.of(context).unfocus(); //Hide keyboard
      Navigator.of(context)
          .pushNamedAndRemoveUntil(Routes.home, (Routes) => false);

      _showErrorMessage('Sign in sukses');
      _loggedIn();
    }
  }

  _loggedIn() {
    SharedPreferences.getInstance().then((value) {
      value.setBool(Preferences.is_logged_in, true);
    });
  }

  _showErrorMessage(String message) {
    Flushbar(
      message: message,
//      title: "Error",
      leftBarIndicatorColor: Colors.red,
      flushbarStyle: FlushbarStyle.FLOATING,
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      duration: Duration(seconds: 2),
    )..show(context);
  }
}
