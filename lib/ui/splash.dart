import 'dart:async';

import 'package:flutter/material.dart';
import 'package:login/constants/preferences.dart';
import 'package:login/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Text("Selamat Datang",
      style: TextStyle(
            decoration: TextDecoration.none,
            color: Colors.black,
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w900,
            fontSize: 40),
    );
  }

  startTimer(){
    var _duration = Duration(milliseconds: 3000);
    Future.delayed(_duration, navigate);
  }

  navigate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(Preferences.is_logged_in) ?? false){
      Navigator.pushReplacementNamed(context, Routes.home);
    } else{
      Navigator.pushReplacementNamed(context, Routes.login);
    }
  }
}
