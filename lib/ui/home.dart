import 'package:flutter/material.dart';
import 'package:login/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/preferences.dart';

class HomeScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Text("HomeScreen"),
    );
  }

  Widget _buildAppBar(){
    return AppBar(
      title: Text("Home"),
      actions: _buildActions(),
    );
  }

  List<Widget> _buildActions(){
    return <Widget>[
      _buildLogoutButton()
    ];
  }

  Widget _buildLogoutButton(){
    return IconButton(
      icon: Icon(Icons.lock_open),
      onPressed: (){
        _loggedOut();
        Navigator.pushReplacementNamed(context, Routes.login);
      },
    );
  }

  _loggedOut(){
    SharedPreferences.getInstance().then((value){
      value.setBool(Preferences.is_logged_in, false);
    });
  }

}
