import 'dart:html';

import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final String hint;
  final String errorText;
  final TextInputType inputType;
  final TextEditingController textController;
  final EdgeInsets padding;
  final ValueChanged onSubmitted;
  final ValueChanged onChanged;
  final IconData icon;

  const TextFieldWidget(
      {Key key,
      this.hint,
      this.errorText,
      this.inputType,
      this.textController,
      this.padding,
      this.onSubmitted,
      this.onChanged,
      this.icon, })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: TextFormField(
        controller: textController,
        onFieldSubmitted: onSubmitted,
        onChanged: onChanged,
        keyboardType: inputType,
        decoration: InputDecoration(
            hintText: hint,
            errorText: errorText,
            icon: Icon(icon)
        ),
      ),
    );
  }
}
