import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inject/inject.dart';
import 'package:login/di/app_component.dart';
import 'package:login/di/modules/preference_module.dart';
import 'package:login/routes.dart';
import 'package:login/ui/splash.dart';

AppComponent appComponent;

void main()  async {
  WidgetsFlutterBinding.ensureInitialized();
  final appComponent = await AppComponent.create(PreferenceModule());
  runApp(appComponent.app);
}

@provide
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Login and Register",
      routes: Routes.routes,
      home: SplashScreen(),
    );
  }
}

