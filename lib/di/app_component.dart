import 'package:inject/inject.dart';
import 'package:login/di/modules/preference_module.dart';
import 'package:login/main.dart';

import 'app_component.inject.dart' as g;

@Injector(const [PreferenceModule])
abstract class AppComponent {
  @provide
  MyApp get app;

  static Future<AppComponent> create(PreferenceModule preferenceModule,) async {
    return await g.AppComponent$Injector.create(
      preferenceModule,
    );
  }
}
