
import 'package:inject/inject.dart';
import 'package:login/data/sharedprefs/shared_preference_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
class PreferenceModule{
  //DI variables:----------------------------------------
  Future<SharedPreferences> sharedPref;

  //constructor------------------------------------------
  // Note: Do not change the order in which providers are called, it might cause
  // some issues
  PreferenceModule(){
    sharedPref = provideSharedPreferences();
  }

  //DI Providers:-----------------------------------------
  @provide
  @singleton
  @asynchronous
  Future<SharedPreferences> provideSharedPreferences(){
    return SharedPreferences.getInstance();
  }

  @provide
  @singleton
  SharedPreferenceHelper provideSharedPreferenceHelper(){
    return SharedPreferenceHelper(sharedPref);
  }
}