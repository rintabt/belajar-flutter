// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_error_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$FormErrorStore on _FormErrorStore, Store {
  Computed<bool> _$hasErrorsInLoginComputed;

  @override
  bool get hasErrorsInLogin => (_$hasErrorsInLoginComputed ??=
          Computed<bool>(() => super.hasErrorsInLogin))
      .value;

  final _$nrpAtom = Atom(name: '_FormErrorStore.nrp');

  @override
  String get nrp {
    _$nrpAtom.context.enforceReadPolicy(_$nrpAtom);
    _$nrpAtom.reportObserved();
    return super.nrp;
  }

  @override
  set nrp(String value) {
    _$nrpAtom.context.conditionallyRunInAction(() {
      super.nrp = value;
      _$nrpAtom.reportChanged();
    }, _$nrpAtom, name: '${_$nrpAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_FormErrorStore.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }
}
