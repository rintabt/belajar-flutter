// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$FormStore on _FormStore, Store {
  Computed<bool> _$canLoginComputed;

  @override
  bool get canLogin =>
      (_$canLoginComputed ??= Computed<bool>(() => super.canLogin)).value;

  final _$nrpAtom = Atom(name: '_FormStore.nrp');

  @override
  String get nrp {
    _$nrpAtom.context.enforceReadPolicy(_$nrpAtom);
    _$nrpAtom.reportObserved();
    return super.nrp;
  }

  @override
  set nrp(String value) {
    _$nrpAtom.context.conditionallyRunInAction(() {
      super.nrp = value;
      _$nrpAtom.reportChanged();
    }, _$nrpAtom, name: '${_$nrpAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_FormStore.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }

  final _$_FormStoreActionController = ActionController(name: '_FormStore');

  @override
  dynamic setNrp(String value) {
    final _$actionInfo = _$_FormStoreActionController.startAction();
    try {
      return super.setNrp(value);
    } finally {
      _$_FormStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setPassword(String value) {
    final _$actionInfo = _$_FormStoreActionController.startAction();
    try {
      return super.setPassword(value);
    } finally {
      _$_FormStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic validateNrp(String value) {
    final _$actionInfo = _$_FormStoreActionController.startAction();
    try {
      return super.validateNrp(value);
    } finally {
      _$_FormStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic validatePassword(String value) {
    final _$actionInfo = _$_FormStoreActionController.startAction();
    try {
      return super.validatePassword(value);
    } finally {
      _$_FormStoreActionController.endAction(_$actionInfo);
    }
  }
}
