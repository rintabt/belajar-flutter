import 'package:mobx/mobx.dart';
import 'package:validators/validators.dart';

import 'form_error_store.dart';

part 'form_store.g.dart';

class FormStore = _FormStore with _$FormStore;

abstract class _FormStore with Store{
  final FormErrorStore formErrorStore = FormErrorStore();

  _FormStore() {
    _setupValidations();
  }

  // disposers:-----------------------------------------------------------------
  List<ReactionDisposer> _disposers;

  void _setupValidations() {
    _disposers = [
      reaction((_) => nrp, validateNrp),
      reaction((_) => password, validatePassword),
    ];
  }

  @observable
  String nrp = '';

  @observable
  String password = '';

  @computed
  bool get canLogin =>
      !formErrorStore.hasErrorsInLogin && nrp.isNotEmpty && password.isNotEmpty;

  @action
  setNrp(String value) => nrp = value;

  @action
  setPassword(String value) => password = value;

  @action
  validateNrp(String value){
    if (value.isEmpty){
      formErrorStore.nrp = "NRP belum diisi";
    } else {
      formErrorStore.nrp = null;
    }
  }

  @action
  validatePassword(String value){
    if (value.isEmpty){
      formErrorStore.password = "Password belum diisi";
    } else {
      formErrorStore.password = null;
    }
  }

}
