
import 'package:login/constants/preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper{
  //sharedpref instance
  final Future<SharedPreferences> _sharedPreferences;

  SharedPreferenceHelper(this._sharedPreferences);

  Future<bool> get isLoggedIn async{
    return _sharedPreferences.then((pref){
      return pref.getString(Preferences.is_logged_in) ?? false;
  });
  }

  Future<void> saveAuthToken(String authToken) async{
    return _sharedPreferences.then((pref)
        => pref.setString(Preferences.auth_token, authToken));
  }

  Future<void> removeAuthToken() async{
    return _sharedPreferences.then((pref)
        => pref.remove(Preferences.auth_token));
  }
}